/********************************************************************************
** Form generated from reading UI file 'cpustresswidget.ui'
**
** Created: Wed Apr 16 19:14:46 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CPUSTRESSWIDGET_H
#define UI_CPUSTRESSWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QTextEdit>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_cpustressWidget
{
public:
    QRadioButton *L1;
    QRadioButton *L2;
    QRadioButton *rijndael;
    QRadioButton *branch;
    QRadioButton *instructionsfp;
    QRadioButton *operationsfp;
    QRadioButton *idleunitsfp;
    QLabel *memory;
    QLabel *floatingpointlabel;
    QPushButton *run;
    QPushButton *exit;
    QLabel *cpuinfolabel;
    QLabel *outputLabel;
    QTextEdit *output;
    QPushButton *clear;
    QLabel *vendorLabel;
    QLineEdit *vendorLine;
    QLabel *cpunameLabel;
    QLineEdit *cpunameLine;
    QLabel *logicalcpuLabel;
    QLineEdit *logicalcpuLine;
    QLabel *coreLabel;
    QLineEdit *coreLine;
    QLabel *htLabel;
    QLineEdit *htLine;

    void setupUi(QWidget *cpustressWidget)
    {
        if (cpustressWidget->objectName().isEmpty())
            cpustressWidget->setObjectName(QString::fromUtf8("cpustressWidget"));
        cpustressWidget->setWindowModality(Qt::WindowModal);
        cpustressWidget->resize(800, 600);
        cpustressWidget->setMinimumSize(QSize(800, 600));
        cpustressWidget->setMaximumSize(QSize(800, 600));
        cpustressWidget->setContextMenuPolicy(Qt::NoContextMenu);
        QIcon icon;
        icon.addFile(QString::fromUtf8("../../Scaricati/twitter2.jpg"), QSize(), QIcon::Normal, QIcon::Off);
        cpustressWidget->setWindowIcon(icon);
        L1 = new QRadioButton(cpustressWidget);
        L1->setObjectName(QString::fromUtf8("L1"));
        L1->setGeometry(QRect(530, 200, 101, 17));
        QFont font;
        font.setPointSize(12);
        L1->setFont(font);
        L1->setMouseTracking(true);
        L1->setFocusPolicy(Qt::NoFocus);
        L2 = new QRadioButton(cpustressWidget);
        L2->setObjectName(QString::fromUtf8("L2"));
        L2->setGeometry(QRect(530, 230, 101, 16));
        L2->setFont(font);
        L2->setMouseTracking(true);
        L2->setFocusPolicy(Qt::NoFocus);
        rijndael = new QRadioButton(cpustressWidget);
        rijndael->setObjectName(QString::fromUtf8("rijndael"));
        rijndael->setGeometry(QRect(480, 310, 311, 22));
        rijndael->setFont(font);
        rijndael->setMouseTracking(true);
        rijndael->setFocusPolicy(Qt::NoFocus);
        rijndael->setAutoExclusive(true);
        branch = new QRadioButton(cpustressWidget);
        branch->setObjectName(QString::fromUtf8("branch"));
        branch->setGeometry(QRect(480, 270, 291, 22));
        branch->setFont(font);
        branch->setMouseTracking(true);
        branch->setFocusPolicy(Qt::NoFocus);
        branch->setAutoExclusive(true);
        instructionsfp = new QRadioButton(cpustressWidget);
        instructionsfp->setObjectName(QString::fromUtf8("instructionsfp"));
        instructionsfp->setGeometry(QRect(530, 70, 116, 22));
        instructionsfp->setFont(font);
        instructionsfp->setMouseTracking(false);
        instructionsfp->setFocusPolicy(Qt::NoFocus);
        instructionsfp->setChecked(false);
        instructionsfp->setAutoExclusive(true);
        operationsfp = new QRadioButton(cpustressWidget);
        operationsfp->setObjectName(QString::fromUtf8("operationsfp"));
        operationsfp->setGeometry(QRect(530, 100, 116, 22));
        operationsfp->setFont(font);
        operationsfp->setMouseTracking(true);
        operationsfp->setFocusPolicy(Qt::NoFocus);
        idleunitsfp = new QRadioButton(cpustressWidget);
        idleunitsfp->setObjectName(QString::fromUtf8("idleunitsfp"));
        idleunitsfp->setGeometry(QRect(530, 130, 116, 22));
        idleunitsfp->setFont(font);
        idleunitsfp->setMouseTracking(true);
        idleunitsfp->setFocusPolicy(Qt::NoFocus);
        memory = new QLabel(cpustressWidget);
        memory->setObjectName(QString::fromUtf8("memory"));
        memory->setGeometry(QRect(480, 170, 181, 17));
        QFont font1;
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setWeight(75);
        memory->setFont(font1);
        floatingpointlabel = new QLabel(cpustressWidget);
        floatingpointlabel->setObjectName(QString::fromUtf8("floatingpointlabel"));
        floatingpointlabel->setGeometry(QRect(480, 40, 211, 17));
        floatingpointlabel->setFont(font1);
        run = new QPushButton(cpustressWidget);
        run->setObjectName(QString::fromUtf8("run"));
        run->setGeometry(QRect(520, 360, 98, 27));
        run->setFont(font1);
        run->setFlat(false);
        exit = new QPushButton(cpustressWidget);
        exit->setObjectName(QString::fromUtf8("exit"));
        exit->setGeometry(QRect(640, 360, 98, 27));
        cpuinfolabel = new QLabel(cpustressWidget);
        cpuinfolabel->setObjectName(QString::fromUtf8("cpuinfolabel"));
        cpuinfolabel->setGeometry(QRect(40, 40, 66, 17));
        cpuinfolabel->setFont(font1);
        outputLabel = new QLabel(cpustressWidget);
        outputLabel->setObjectName(QString::fromUtf8("outputLabel"));
        outputLabel->setGeometry(QRect(30, 380, 66, 31));
        outputLabel->setFont(font1);
        output = new QTextEdit(cpustressWidget);
        output->setObjectName(QString::fromUtf8("output"));
        output->setGeometry(QRect(20, 420, 761, 161));
        QPalette palette;
        QBrush brush(QColor(85, 255, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        QBrush brush1(QColor(0, 0, 0, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        QBrush brush2(QColor(161, 160, 159, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush2);
        QBrush brush3(QColor(255, 255, 255, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush3);
        output->setPalette(palette);
        output->setReadOnly(true);
        output->setTextInteractionFlags(Qt::NoTextInteraction);
        clear = new QPushButton(cpustressWidget);
        clear->setObjectName(QString::fromUtf8("clear"));
        clear->setGeometry(QRect(730, 400, 51, 20));
        vendorLabel = new QLabel(cpustressWidget);
        vendorLabel->setObjectName(QString::fromUtf8("vendorLabel"));
        vendorLabel->setGeometry(QRect(70, 80, 61, 31));
        vendorLabel->setFont(font);
        vendorLine = new QLineEdit(cpustressWidget);
        vendorLine->setObjectName(QString::fromUtf8("vendorLine"));
        vendorLine->setGeometry(QRect(130, 80, 113, 31));
        QFont font2;
        font2.setPointSize(10);
        font2.setBold(true);
        font2.setWeight(75);
        vendorLine->setFont(font2);
        vendorLine->setMouseTracking(false);
        vendorLine->setFocusPolicy(Qt::NoFocus);
        vendorLine->setAcceptDrops(false);
        vendorLine->setAutoFillBackground(true);
        vendorLine->setFrame(true);
        vendorLine->setReadOnly(true);
        cpunameLabel = new QLabel(cpustressWidget);
        cpunameLabel->setObjectName(QString::fromUtf8("cpunameLabel"));
        cpunameLabel->setGeometry(QRect(50, 130, 141, 31));
        cpunameLabel->setFont(font);
        cpunameLine = new QLineEdit(cpustressWidget);
        cpunameLine->setObjectName(QString::fromUtf8("cpunameLine"));
        cpunameLine->setGeometry(QRect(130, 130, 331, 31));
        cpunameLine->setFont(font2);
        cpunameLine->setMouseTracking(false);
        cpunameLine->setFocusPolicy(Qt::NoFocus);
        cpunameLine->setAcceptDrops(false);
        cpunameLine->setMaxLength(49);
        cpunameLine->setFrame(true);
        cpunameLine->setEchoMode(QLineEdit::Normal);
        cpunameLine->setDragEnabled(true);
        cpunameLine->setReadOnly(true);
        logicalcpuLabel = new QLabel(cpustressWidget);
        logicalcpuLabel->setObjectName(QString::fromUtf8("logicalcpuLabel"));
        logicalcpuLabel->setGeometry(QRect(30, 180, 101, 31));
        logicalcpuLabel->setFont(font);
        logicalcpuLine = new QLineEdit(cpustressWidget);
        logicalcpuLine->setObjectName(QString::fromUtf8("logicalcpuLine"));
        logicalcpuLine->setGeometry(QRect(130, 180, 31, 31));
        logicalcpuLine->setFont(font2);
        logicalcpuLine->setFocusPolicy(Qt::NoFocus);
        logicalcpuLine->setAcceptDrops(false);
        logicalcpuLine->setReadOnly(true);
        coreLabel = new QLabel(cpustressWidget);
        coreLabel->setObjectName(QString::fromUtf8("coreLabel"));
        coreLabel->setGeometry(QRect(50, 230, 81, 31));
        coreLabel->setFont(font);
        coreLine = new QLineEdit(cpustressWidget);
        coreLine->setObjectName(QString::fromUtf8("coreLine"));
        coreLine->setGeometry(QRect(130, 230, 31, 31));
        coreLine->setFont(font2);
        coreLine->setMouseTracking(false);
        coreLine->setFocusPolicy(Qt::NoFocus);
        coreLine->setAcceptDrops(false);
        coreLine->setReadOnly(true);
        htLabel = new QLabel(cpustressWidget);
        htLabel->setObjectName(QString::fromUtf8("htLabel"));
        htLabel->setGeometry(QRect(10, 280, 131, 31));
        htLabel->setFont(font);
        htLine = new QLineEdit(cpustressWidget);
        htLine->setObjectName(QString::fromUtf8("htLine"));
        htLine->setGeometry(QRect(130, 280, 51, 31));
        htLine->setFont(font2);
        htLine->setMouseTracking(false);
        htLine->setFocusPolicy(Qt::NoFocus);
        htLine->setAcceptDrops(false);
        htLine->setAutoFillBackground(false);
        htLine->setReadOnly(true);

        retranslateUi(cpustressWidget);
        QObject::connect(clear, SIGNAL(clicked()), output, SLOT(clear()));

        QMetaObject::connectSlotsByName(cpustressWidget);
    } // setupUi

    void retranslateUi(QWidget *cpustressWidget)
    {
        cpustressWidget->setWindowTitle(QApplication::translate("cpustressWidget", "CpuStress", 0, QApplication::UnicodeUTF8));
        L1->setText(QApplication::translate("cpustressWidget", "L1 Cache", 0, QApplication::UnicodeUTF8));
        L2->setText(QApplication::translate("cpustressWidget", "L2 Cache", 0, QApplication::UnicodeUTF8));
        rijndael->setText(QApplication::translate("cpustressWidget", "Rijndael Cryptography Benchmark", 0, QApplication::UnicodeUTF8));
        branch->setText(QApplication::translate("cpustressWidget", "Branch Misprediction Benchmark", 0, QApplication::UnicodeUTF8));
        instructionsfp->setText(QApplication::translate("cpustressWidget", "Instructions", 0, QApplication::UnicodeUTF8));
        operationsfp->setText(QApplication::translate("cpustressWidget", "Operations", 0, QApplication::UnicodeUTF8));
        idleunitsfp->setText(QApplication::translate("cpustressWidget", "Idle units", 0, QApplication::UnicodeUTF8));
        memory->setText(QApplication::translate("cpustressWidget", "Cache Miss Benchmark", 0, QApplication::UnicodeUTF8));
        floatingpointlabel->setText(QApplication::translate("cpustressWidget", "Floating Point Benchmark", 0, QApplication::UnicodeUTF8));
        run->setText(QApplication::translate("cpustressWidget", "RUN!", 0, QApplication::UnicodeUTF8));
        exit->setText(QApplication::translate("cpustressWidget", "exit", 0, QApplication::UnicodeUTF8));
        cpuinfolabel->setText(QApplication::translate("cpustressWidget", "CPU Info", 0, QApplication::UnicodeUTF8));
        outputLabel->setText(QApplication::translate("cpustressWidget", "Output", 0, QApplication::UnicodeUTF8));
        clear->setText(QApplication::translate("cpustressWidget", "clear", 0, QApplication::UnicodeUTF8));
        vendorLabel->setText(QApplication::translate("cpustressWidget", "Vendor", 0, QApplication::UnicodeUTF8));
        vendorLine->setText(QString());
        cpunameLabel->setText(QApplication::translate("cpustressWidget", "CPU name", 0, QApplication::UnicodeUTF8));
        cpunameLine->setText(QString());
        logicalcpuLabel->setText(QApplication::translate("cpustressWidget", "Logical CPUs", 0, QApplication::UnicodeUTF8));
        logicalcpuLine->setText(QString());
        coreLabel->setText(QApplication::translate("cpustressWidget", "CPU cores", 0, QApplication::UnicodeUTF8));
        coreLine->setText(QString());
        htLabel->setText(QApplication::translate("cpustressWidget", "HyperThreading", 0, QApplication::UnicodeUTF8));
        htLine->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class cpustressWidget: public Ui_cpustressWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CPUSTRESSWIDGET_H
