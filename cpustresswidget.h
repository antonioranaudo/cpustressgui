#ifndef CPUSTRESSWIDGET_H
#define CPUSTRESSWIDGET_H

#include <QWidget>
#include "dialog.h"

namespace Ui {
class cpustressWidget;
}

class cpustressWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit cpustressWidget(QWidget *parent = 0);
    ~cpustressWidget();

public slots:

    void setVendor(QString &producer);
    void setName(QString &name);
    void setLogicalCPU(int &logicalCPU);
    void setCore(int &cores);
    void setHT(unsigned &hyperThreads);
    
    void setInstruction(int &fp_ins);
    void setOperations(int &fp_ops);
    void setIdleunit(int fp_idle);
    void setBranch(int &brmis);
    void setL1Cachemiss(int &l1cm);
    void setL2Cachemiss(int &l2cm);
    void setRijndael(int &fp_ins, int &l1cm, int &l2cm);

private slots:

    void on_run_clicked();

    void on_exit_clicked();

    void on_instructionsfp_clicked();

    void on_operationsfp_clicked();

    void on_idleunitsfp_clicked();

    void on_L1_clicked();

    void on_L2_clicked();

    void on_branch_clicked();

    void on_rijndael_clicked();

    void readValues();

private:
    Ui::cpustressWidget *ui;
    int radio, execution, idle, operations;
    bool two;
    Dialog input;
};

#endif // CPUSTRESSWIDGET_H
