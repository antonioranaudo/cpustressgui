#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();
    void get3Input(int &execution, int &idle, int &operations);
    void get2Input(int &execution, int &idle);

private slots:

    void operationsDisabled();
    void operationsEnabled();


private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
