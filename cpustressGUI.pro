#-------------------------------------------------
#
# Project created by QtCreator 2014-04-12T12:54:19
#
#-------------------------------------------------

QT       += core gui

TARGET = cpustressGUI
TEMPLATE = app


SOURCES += main.cpp\
        cpustresswidget.cpp \
    dialog.cpp

HEADERS  += cpustresswidget.h \
    dialog.h

FORMS    += cpustresswidget.ui \
    dialog.ui

LIBS += check.o function.o rijndael-alg-fst.o rijndael-api-fst.o rijndael-test-fst.o /usr/local/lib/libpapi.a -lgomp -lpthread

OTHER_FILES +=
