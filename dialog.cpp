#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::get3Input(int &execution, int &idle, int &operations)  //prende i valori dalle tre spinbox
{
    execution=ui->executionBox->value();
    idle=ui->idleBox->value();
    operations=ui->operationsBox->value();
}

void Dialog::get2Input(int &execution, int &idle) //prende i valori dalle 2 spinbox
{
    execution=ui->executionBox->value();
    idle=ui->idleBox->value();
}

void Dialog::operationsDisabled() //disabilita la spinbox delle operazioni
{
    ui->operationsBox->setDisabled(true);
}

void Dialog::operationsEnabled() //abilita la spinbox delle operazioni
{
    ui->operationsBox->setEnabled(true);
}
