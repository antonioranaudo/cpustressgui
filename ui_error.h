/********************************************************************************
** Form generated from reading UI file 'error.ui'
**
** Created: Thu Apr 17 14:53:04 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ERROR_H
#define UI_ERROR_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_error
{
public:
    QLabel *papi;
    QPushButton *exiterror;

    void setupUi(QDialog *error)
    {
        if (error->objectName().isEmpty())
            error->setObjectName(QString::fromUtf8("error"));
        error->setWindowModality(Qt::WindowModal);
        error->resize(323, 197);
        QIcon icon;
        icon.addFile(QString::fromUtf8("../../Scaricati/twitter2.jpg"), QSize(), QIcon::Normal, QIcon::Off);
        error->setWindowIcon(icon);
        error->setModal(true);
        papi = new QLabel(error);
        papi->setObjectName(QString::fromUtf8("papi"));
        papi->setGeometry(QRect(25, 50, 281, 41));
        QFont font;
        font.setPointSize(18);
        font.setBold(true);
        font.setWeight(75);
        papi->setFont(font);
        exiterror = new QPushButton(error);
        exiterror->setObjectName(QString::fromUtf8("exiterror"));
        exiterror->setGeometry(QRect(110, 140, 98, 27));

        retranslateUi(error);

        QMetaObject::connectSlotsByName(error);
    } // setupUi

    void retranslateUi(QDialog *error)
    {
        error->setWindowTitle(QApplication::translate("error", "ERROR!", 0, QApplication::UnicodeUTF8));
        papi->setText(QApplication::translate("error", "PAPI non INSTALLATO!!", 0, QApplication::UnicodeUTF8));
        exiterror->setText(QApplication::translate("error", "EXIT", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class error: public Ui_error {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ERROR_H
