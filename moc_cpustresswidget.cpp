/****************************************************************************
** Meta object code from reading C++ file 'cpustresswidget.h'
**
** Created: Thu Apr 17 15:08:26 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "cpustresswidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'cpustresswidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_cpustressWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      22,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      26,   17,   16,   16, 0x0a,
      51,   46,   16,   16, 0x0a,
      80,   69,   16,   16, 0x0a,
     106,  100,   16,   16, 0x0a,
     133,  120,   16,   16, 0x0a,
     153,  146,   16,   16, 0x0a,
     181,  174,   16,   16, 0x0a,
     209,  201,   16,   16, 0x0a,
     232,  226,   16,   16, 0x0a,
     253,  248,   16,   16, 0x0a,
     279,  274,   16,   16, 0x0a,
     317,  300,   16,   16, 0x0a,
     345,   16,   16,   16, 0x08,
     362,   16,   16,   16, 0x08,
     380,   16,   16,   16, 0x08,
     408,   16,   16,   16, 0x08,
     434,   16,   16,   16, 0x08,
     459,   16,   16,   16, 0x08,
     475,   16,   16,   16, 0x08,
     491,   16,   16,   16, 0x08,
     511,   16,   16,   16, 0x08,
     533,   16,   16,   16, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_cpustressWidget[] = {
    "cpustressWidget\0\0producer\0setVendor(QString&)\0"
    "name\0setName(QString&)\0logicalCPU\0"
    "setLogicalCPU(int&)\0cores\0setCore(int&)\0"
    "hyperThreads\0setHT(uint&)\0fp_ins\0"
    "setInstruction(int&)\0fp_ops\0"
    "setOperations(int&)\0fp_idle\0"
    "setIdleunit(int)\0brmis\0setBranch(int&)\0"
    "l1cm\0setL1Cachemiss(int&)\0l2cm\0"
    "setL2Cachemiss(int&)\0fp_ins,l1cm,l2cm\0"
    "setRijndael(int&,int&,int&)\0"
    "on_run_clicked()\0on_exit_clicked()\0"
    "on_instructionsfp_clicked()\0"
    "on_operationsfp_clicked()\0"
    "on_idleunitsfp_clicked()\0on_L1_clicked()\0"
    "on_L2_clicked()\0on_branch_clicked()\0"
    "on_rijndael_clicked()\0readValues()\0"
};

void cpustressWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        cpustressWidget *_t = static_cast<cpustressWidget *>(_o);
        switch (_id) {
        case 0: _t->setVendor((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->setName((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->setLogicalCPU((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->setCore((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->setHT((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 5: _t->setInstruction((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->setOperations((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->setIdleunit((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->setBranch((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->setL1Cachemiss((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->setL2Cachemiss((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->setRijndael((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 12: _t->on_run_clicked(); break;
        case 13: _t->on_exit_clicked(); break;
        case 14: _t->on_instructionsfp_clicked(); break;
        case 15: _t->on_operationsfp_clicked(); break;
        case 16: _t->on_idleunitsfp_clicked(); break;
        case 17: _t->on_L1_clicked(); break;
        case 18: _t->on_L2_clicked(); break;
        case 19: _t->on_branch_clicked(); break;
        case 20: _t->on_rijndael_clicked(); break;
        case 21: _t->readValues(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData cpustressWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject cpustressWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_cpustressWidget,
      qt_meta_data_cpustressWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &cpustressWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *cpustressWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *cpustressWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_cpustressWidget))
        return static_cast<void*>(const_cast< cpustressWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int cpustressWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
