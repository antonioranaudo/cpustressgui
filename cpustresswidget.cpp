#include "cpustresswidget.h"
#include "ui_cpustresswidget.h"
#include <QDebug>

cpustressWidget::cpustressWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::cpustressWidget)
{
    ui->setupUi(this);
    connect(ui->exit,SIGNAL(clicked()),this,SLOT(on_exit_clicked())); //al click del pulsante EXIT esce dall'app
    connect(ui->instructionsfp,SIGNAL(clicked()),this,SLOT(on_instructionsfp_clicked())); //abilita l'esecuzione del bench sulle ISTRUZIONI fp
    connect(ui->instructionsfp,SIGNAL(clicked()),&input,SLOT(operationsEnabled())); //abilita nella dialog la spinbox delle operazioni
    connect(ui->operationsfp,SIGNAL(clicked()),this,SLOT(on_operationsfp_clicked())); //abilita l'esecuzione del bench sulle OPERAZIONI fp
    connect(ui->operationsfp,SIGNAL(clicked()),&input,SLOT(operationsEnabled())); //abilita nella dialog la spinbox delle operazioni
    connect(ui->idleunitsfp,SIGNAL(clicked()),this,SLOT(on_idleunitsfp_clicked())); //abilita l'esecuzione del bench sulle IDLE UNITS fp
    connect(ui->idleunitsfp,SIGNAL(clicked()),&input,SLOT(operationsEnabled())); //abilita nella dialog la spinbox delle operazioni
    connect(ui->L1,SIGNAL(clicked()),this,SLOT(on_L1_clicked())); //abilita l'esecuzione del bench sugli L1 Cache Miss
    connect(ui->L1,SIGNAL(clicked()),&input,SLOT(operationsDisabled())); //disabilita nella dialog la spinbox delle operazioni
    connect(ui->L2,SIGNAL(clicked()),this,SLOT(on_L2_clicked())); //abilita l'esecuzione del bench sugli L2 Cache Miss
    connect(ui->L2,SIGNAL(clicked()),&input,SLOT(operationsDisabled())); //disabilita nella dialog la spinbox delle operazioni
    connect(ui->branch,SIGNAL(clicked()),this,SLOT(on_branch_clicked())); //abilita l'esecuzione del bench sul BRANCH MISPREDICTION
    connect(ui->branch,SIGNAL(clicked()),&input,SLOT(operationsDisabled())); //disabilita nella dialog la spinbox delle operazioni
    connect(ui->rijndael,SIGNAL(clicked()),this,SLOT(on_rijndael_clicked())); //abilita l'esecuzione del bench sull'algoritmo di crittografia RIJNDAEL
    connect(&input,SIGNAL(accepted()),this,SLOT(readValues())); //abilita la lettura degli input immessi nella dialog
}

cpustressWidget::~cpustressWidget()
{
    delete ui;
}

void cpustressWidget::on_run_clicked()
{
    switch(radio) {
    case(1): { // Floating Point Instructions
        qDebug() << "floating point istructions";
        two=false;
        input.show();
        }
        break;
    case(2): { // Floating Point Operations
        qDebug() << "floating point operations";
        two=false;
        input.show();
        }
        break;
    case(3): { // Floating Point Idle Units
        qDebug() << "floating point idle units";
        two=false;
        input.show();
        }
        break;
    case(4): { // L1 Cache Miss
        qDebug() << "L1 cache";
        two=true;
        input.show();
        }
        break;
    case(5): { // L2 Cache Miss
        qDebug() << "L2 cache";
        two=true;
        input.show();
        }
        break;
    case(6): { // Branch Misprediction
        qDebug() << "branch";
        two=true;
        input.show();
        }
        break;
    case(7): { // Rijndael
        qDebug() << "rijndael";
        }
        break;
    }
}

void cpustressWidget::readValues() { //lettura dei valori di input dalla dialog
    if(two){
        input.get2Input(execution,idle);
        qDebug() << execution << endl << idle;
    }
    else {
        input.get3Input(execution,idle,operations);
        qDebug() << execution << endl << idle << endl << operations;
    }
}

void cpustressWidget::on_exit_clicked() {
    exit(0);
}

void cpustressWidget::on_instructionsfp_clicked() {
    radio=1;
}

void cpustressWidget::on_operationsfp_clicked() {
    radio=2;
}

void cpustressWidget::on_idleunitsfp_clicked() {
    radio=3;
}

void cpustressWidget::on_L1_clicked() {
    radio=4;
}

void cpustressWidget::on_L2_clicked() {
    radio=5;
}

void cpustressWidget::on_branch_clicked() {
    radio=6;
}

void cpustressWidget::on_rijndael_clicked() {
    radio=7;
}

// metodi set per riempire i campi di CPUinfo

void cpustressWidget::setVendor(QString &producer) {
    ui->vendorLine->setText(producer);
}
void cpustressWidget::setName(QString &name) {
    ui->cpunameLine->setText(name);
}
void cpustressWidget::setLogicalCPU(int &logicalCPU) {
    QString logical = QString::number(logicalCPU);
    ui->logicalcpuLine->setText(logical);
}
void cpustressWidget::setCore(int &cores) {
    QString core = QString::number(cores);
    ui->coreLine->setText(core);
}
void cpustressWidget::setHT(unsigned &hyperThreads) {
    ui->htLine->setText((hyperThreads ? "YES" : "NO"));
}

//metodi per l'abilitazione dei pulsanti dei bench, sulla base dei check dei contatori PAPI

void cpustressWidget::setInstruction(int &fp_ins) {
    if(fp_ins==1) {
        ui->instructionsfp->setEnabled(true);
    }
    else ui->instructionsfp->setDisabled(true);
}
void cpustressWidget::setOperations(int &fp_ops) {
    if(fp_ops==1) {
        ui->instructionsfp->setEnabled(true);
    }
    else ui->instructionsfp->setDisabled(true);
}
void cpustressWidget::setIdleunit(int fp_idle) {
    if(fp_idle==1) {
        ui->idleunitsfp->setEnabled(true);
    }
    else ui->idleunitsfp->setDisabled(true);
}
void cpustressWidget::setBranch(int &brmis) {
    if(brmis==1) {
        ui->branch->setEnabled(true);
    }
    else ui->branch->setDisabled(true);
}
void cpustressWidget::setL1Cachemiss(int &l1cm) {
    if(l1cm==1) {
        ui->L1->setEnabled(true);
    }
    else ui->L1->setDisabled(true);
}
void cpustressWidget::setL2Cachemiss(int &l2cm) {
    if(l2cm==1) {
        ui->L2->setEnabled(true);
    }
    else ui->L2->setDisabled(true);
}
void cpustressWidget::setRijndael(int &fp_ins, int &l1cm, int &l2cm) {
    if(fp_ins==1 || l1cm==1 || l2cm==1) {
        ui->rijndael->setEnabled(true);
    }
    else ui->rijndael->setDisabled(true);
}
