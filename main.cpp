#include <QtGui/QApplication>
#include "cpustresswidget.h"
#include "function.h"
#include "check.h"
#include <QDebug>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    cpustressWidget w;

    // --- Raccolta info sulla CPU ---

    unsigned regs[4];
    char vendor[12];
    char name[49];

    cpuVendor(regs,vendor); //ottiene il produttore della CPU
    QString producer(vendor); //da char a QString per passarlo al widget
    w.setVendor(producer);  //setta il produttore da passare al widget

    cpuName(regs,name);     //ottiene il nome completo della CPU
    QString cpuname(name); //da char a QString per passarlo al widget
    w.setName(cpuname);     //setta il nome da passare al widget

    int logicalCPU=cpuLogical(regs);    //ottiene il numero di cpu logiche
    w.setLogicalCPU(logicalCPU);    //setta il numero di cpu logiche da passare al widget

    int cores=cpuCores(regs,vendor);    //ottiene il numero di core
    w.setCore(cores);      //setta il numero di core da passare al widget

    unsigned hyperThreads=cpuHyperThreading(regs,cores,logicalCPU);  //ottiene info sulla presenza dell'HT
    w.setHT(hyperThreads);  //setta la variabile che notifica la presenza dell'HT

    // --- Controlli sui contatori PAPI presenti sulla CPU ---

    int fp_ins = checkfp_ins(); //controllo di PAPI_FP_INS
    w.setInstruction(fp_ins);   //setta la variabile di controllo da passare al widget

    int fp_ops = checkfp_ops(); //controllo di PAPI_FP_OPS
    w.setOperations(fp_ops);    //setta la variabile di controllo da passare al widget

    int fp_idle = checkfpu_idl();   //controllo di PAPI_FPU_IDL
    w.setIdleunit(fp_idle);     //setta la variabile di controllo da passare al widget

    int brmis = checkbr_mis();  //controllo di PAPI_BR_MIS
    w.setBranch(brmis);         //setta la variabile di controllo da passare al widget

    int l1cm = checkl1_tcm();   //controllo di PAPI_L1_TCM
    w.setL1Cachemiss(l1cm);     //setta la variabile di controllo da passare al widget

    int l2cm = checkl2_tcm();   //controllo di PAPI_L2_TCM
    w.setL2Cachemiss(l2cm);     //setta la variabile di controllo da passare al widget

    w.setRijndael(fp_ins,l1cm,l2cm); //setta la variabile di controllo da passare al widget
    w.show();
    return a.exec();
}
