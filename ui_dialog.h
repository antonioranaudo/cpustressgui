/********************************************************************************
** Form generated from reading UI file 'dialog.ui'
**
** Created: Wed Apr 16 17:12:22 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_H
#define UI_DIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QSpinBox>

QT_BEGIN_NAMESPACE

class Ui_Dialog
{
public:
    QDialogButtonBox *buttonBox;
    QLabel *executionLabel;
    QSpinBox *executionBox;
    QLabel *idleLabel;
    QSpinBox *idleBox;
    QLabel *operationsLabel;
    QSpinBox *operationsBox;

    void setupUi(QDialog *Dialog)
    {
        if (Dialog->objectName().isEmpty())
            Dialog->setObjectName(QString::fromUtf8("Dialog"));
        Dialog->setWindowModality(Qt::WindowModal);
        Dialog->resize(270, 320);
        QIcon icon;
        icon.addFile(QString::fromUtf8("../../Scaricati/twitter2.jpg"), QSize(), QIcon::Normal, QIcon::Off);
        Dialog->setWindowIcon(icon);
        buttonBox = new QDialogButtonBox(Dialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(20, 260, 221, 41));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        executionLabel = new QLabel(Dialog);
        executionLabel->setObjectName(QString::fromUtf8("executionLabel"));
        executionLabel->setGeometry(QRect(60, 30, 121, 31));
        QFont font;
        font.setPointSize(12);
        executionLabel->setFont(font);
        executionBox = new QSpinBox(Dialog);
        executionBox->setObjectName(QString::fromUtf8("executionBox"));
        executionBox->setGeometry(QRect(190, 30, 71, 31));
        executionBox->setMinimum(30);
        executionBox->setMaximum(300);
        executionBox->setSingleStep(15);
        idleLabel = new QLabel(Dialog);
        idleLabel->setObjectName(QString::fromUtf8("idleLabel"));
        idleLabel->setGeometry(QRect(100, 89, 71, 31));
        idleLabel->setFont(font);
        idleBox = new QSpinBox(Dialog);
        idleBox->setObjectName(QString::fromUtf8("idleBox"));
        idleBox->setGeometry(QRect(190, 90, 71, 31));
        idleBox->setMinimum(1);
        idleBox->setMaximum(5);
        operationsLabel = new QLabel(Dialog);
        operationsLabel->setObjectName(QString::fromUtf8("operationsLabel"));
        operationsLabel->setGeometry(QRect(10, 150, 171, 31));
        operationsLabel->setFont(font);
        operationsBox = new QSpinBox(Dialog);
        operationsBox->setObjectName(QString::fromUtf8("operationsBox"));
        operationsBox->setGeometry(QRect(190, 150, 71, 31));
        operationsBox->setMinimum(100);
        operationsBox->setMaximum(4000);
        operationsBox->setSingleStep(100);

        retranslateUi(Dialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), Dialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), Dialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(Dialog);
    } // setupUi

    void retranslateUi(QDialog *Dialog)
    {
        Dialog->setWindowTitle(QApplication::translate("Dialog", "Input", 0, QApplication::UnicodeUTF8));
        executionLabel->setText(QApplication::translate("Dialog", "Execution Time", 0, QApplication::UnicodeUTF8));
        executionBox->setSuffix(QApplication::translate("Dialog", "s", 0, QApplication::UnicodeUTF8));
        idleLabel->setText(QApplication::translate("Dialog", "Idle Time", 0, QApplication::UnicodeUTF8));
        idleBox->setSuffix(QApplication::translate("Dialog", "s", 0, QApplication::UnicodeUTF8));
        operationsLabel->setText(QApplication::translate("Dialog", "Number of Operations", 0, QApplication::UnicodeUTF8));
        operationsBox->setSuffix(QApplication::translate("Dialog", "k", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Dialog: public Ui_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_H
